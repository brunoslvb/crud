package visual;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import mail.Mail;
import aula0309.database.ListaDados;
import aula0309.database.Usuario;

public class FrmListUsuarios extends JFrame{
	
	JPanel painelFundo;
    JTable tabela;
    JScrollPane barraRolagem;
	
    private int[] rowTable = null;
    
    private static String[] colunas = {"idUsuario","nome","email","senha","nivel"};
    private String[][] dados;
    
	private Usuario usuario = new Usuario("", "", "", "", "");
	private Vector<String[]> data = new Vector<String[]>();
	
	private JLabel			lblUsuario 	= new JLabel("ID:");
	private JTextField 		txtUsuario 	= new JTextField();
	private JLabel			lblNome 	= new JLabel("Nome:");
	private JTextField 		txtNome 	= new JTextField();
	private JLabel			lblEmail 	= new JLabel("Email:");
	private JTextField 		txtEmail 	= new JTextField();
	private JLabel			lblSenha 	= new JLabel("Senha:");
	private JPasswordField 	txtSenha 	= new JPasswordField();
	private JLabel			lblNivel 	= new JLabel("N�vel:");
	private JTextField 		txtNivel 	= new JTextField();
	private JLabel			lblMsg 		= new JLabel("");
	private JButton			btnOptionInsert 	= new JButton("Cadastrar");
	private JButton			btnOptionUpdate 	= new JButton("Atualizar");
	private JButton			btnOptionDelete 	= new JButton("Deletar");
	private JButton			btnOptionSearch 	= new JButton("Buscar");
	
	private JLabel			lblNomeUp 	= new JLabel("Nome:");
	private JTextField 		txtNomeUp 	= new JTextField();
	private JLabel			lblEmailUp 	= new JLabel("Email:");
	private JTextField 		txtEmailUp 	= new JTextField();
	private JLabel			lblSenhaUp 	= new JLabel("Senha:");
	private JPasswordField 	txtSenhaUp 	= new JPasswordField();
	private JLabel			lblNivelUp 	= new JLabel("N�vel:");
	private JTextField 		txtNivelUp 	= new JTextField();
	
	private JButton			btnInsert 	= new JButton("Cadastrar");
	private JButton			btnUpdate 	= new JButton("Atualizar");
	private JButton			btnSearch 	= new JButton("Buscar");
	
	private JLabel			lblID 	= new JLabel("ID do Usuario: ");
	private JTextField 		txtID 	= new JTextField();

	public FrmListUsuarios() {

		this.dados   =  this.usuario.resultSetToArray(usuario.listall());

		painelFundo = new JPanel();
        painelFundo.setLayout(new GridLayout(1, 1));
        tabela = new JTable(dados, colunas);
        barraRolagem = new JScrollPane(tabela);
        painelFundo.add(barraRolagem); 
         
        getContentPane().add(painelFundo);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 300);
        setVisible(true);
        
        this.setTitle("Lista de usuarios");
		this.setBounds(360, 60, 600, 600);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		btnOptionInsert.setBounds(80, 280, 100, 30);
		btnOptionUpdate.setBounds(240, 280, 100, 30);
		btnOptionDelete.setBounds(400, 280, 100, 30);
		lblMsg.setBounds(120, 500, 350, 30);
		//btnOptionSearch.setBounds(420, 280, 100, 30);
		
        tabela.addMouseListener( new MouseListener() {
        	@Override
	        public void mouseClicked(MouseEvent e) {
	        	rowTable = tabela.getSelectedRows();

	        	int i = rowTable[0];
				
				txtUsuario.setText((String) tabela.getValueAt(i, 0));
				txtNomeUp.setText((String) tabela.getValueAt(i, 1));
				txtEmailUp.setText((String) tabela.getValueAt(i, 2));
				txtSenhaUp.setText((String) tabela.getValueAt(i, 3));
				txtNivelUp.setText((String) tabela.getValueAt(i, 4));
	        	
        	}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}
        });
        
		/*btnOptionSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				//System.out.print(rowTable);
				
				// Limpa os campos
				
				limpaFormInsert();
				limpaFormUpdate();
				
				lblID.setBounds( 220,  350, 100, 30);
				txtID.setBounds( 310, 350, 70, 30);
				btnSearch.setBounds(250, 430, 100, 30);
			}
		});*/

		btnOptionInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				limpaFormSearch();
				limpaFormUpdate();

				lblNome.setBounds	(80, 350, 100, 30);
				txtNome.setBounds	(130, 350, 150, 30);
				lblEmail.setBounds	(290, 350, 100, 30);
				txtEmail.setBounds	(330, 350, 150, 30);
				lblSenha.setBounds	(80, 400, 100, 30);
				txtSenha.setBounds	(130, 400, 150, 30);
				lblNivel.setBounds	(290, 400, 100, 30);
				txtNivel.setBounds	(330, 400, 150, 30);
				btnInsert.setBounds	(220, 450, 150, 30);
				
				txtNome.setText("");
				txtEmail.setText("");
				txtSenha.setText("");
				txtNivel.setText("");

			}
		});
		
		btnOptionUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				limpaFormSearch();
				limpaFormInsert();
				
				lblNomeUp.setBounds	(80, 350, 100, 30);
				txtNomeUp.setBounds	(130, 350, 150, 30);
				lblEmailUp.setBounds(290, 350, 100, 30);
				txtEmailUp.setBounds(330, 350, 150, 30);
				lblSenhaUp.setBounds(80, 400, 100, 30);
				txtSenhaUp.setBounds(130, 400, 150, 30);
				lblNivelUp.setBounds(290, 400, 100, 30);
				txtNivelUp.setBounds(330, 400, 150, 30);
				btnUpdate.setBounds	(220, 450, 150, 30);
			}
		});

		btnOptionDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				System.out.print(rowTable.length);
				
				if(rowTable.length > 0){
				
					for (int i = 0; i < rowTable.length; i++) {
		        		//System.out.print(rowTable[i]);
		        								
						String id = (String) tabela.getValueAt(rowTable[i], 0);
						
						Usuario usuario = new Usuario(id, "", "", "", "");
					
						if(usuario.delete()){
							lblMsg.setText("Usu�rio " + id + "foi exclu�do com sucesso.");
							dispose();
							new FrmListUsuarios();
						} else {
							lblMsg.setText("Erro ao excluir");
							System.out.print("Erro ao excluir");
						}
								        	    
		        	    //DefaultTableModel model = (DefaultTableModel)tabela.getModel();
		                //model.removeRow(2);
		        	    //System.out.print(aux);
		        	    
					}
				} else {
					lblMsg.setText("Selecione alguma linha na tabela para deletar.");
				}
			}
		});

		/*btnSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				ListaDados ld = new ListaDados();
				
				Usuario usuario = new Usuario(txtID.getText(), "", "", "", "");
								
				String[][] dadosFind = usuario.resultSetToArray(usuario.find());
				
				dispose();
				setDados(dadosFind);
				new FrmListUsuarios();
				
			}
		});*/
		
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(isDigit(txtNivel.getText())){
					Usuario usuario = new Usuario("", txtNome.getText(), txtEmail.getText(), txtSenha.getText(), txtNivel.getText());
					usuario.save();
					
					Mail mail = new Mail(txtNome.getText(), txtEmail.getText(), txtSenha.getText());
					mail.sendMail();
					
					dispose();
					new FrmListUsuarios();
				} else {
					lblMsg.setText("O campo de entrada 'Nivel' espera um valor inteiro");
				}
			}
		});
		
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(isDigit(txtNivelUp.getText())){
					Usuario usuario = new Usuario(txtUsuario.getText(), txtNomeUp.getText(), txtEmailUp.getText(), txtSenhaUp.getText(), txtNivelUp.getText());
					usuario.save();
					
					dispose();
					new FrmListUsuarios();
				} else {
					lblMsg.setText("O campo de entrada 'Nivel' espera um valor inteiro");
				}
				
			}
		});
		
        this.add(btnOptionInsert);
        this.add(btnOptionUpdate);
        this.add(btnOptionDelete);
        this.add(btnOptionSearch);
        
		this.add(lblUsuario);
		this.add(txtUsuario);
		this.add(lblNome);
		this.add(txtNome);
		this.add(lblEmail);
		this.add(txtEmail);
		this.add(lblSenha);
		this.add(txtSenha);
		this.add(lblNivel);
		this.add(txtNivel);
		this.add(btnInsert);
		this.add(btnUpdate);
		
		this.add(lblNomeUp);
		this.add(txtNomeUp);
		this.add(lblEmailUp);
		this.add(txtEmailUp);
		this.add(lblSenhaUp);
		this.add(txtSenhaUp);
		this.add(lblNivelUp);
		this.add(txtNivelUp);
		
		this.add(lblMsg);

        this.add(lblID);
        this.add(txtID);
        this.add(btnSearch);
        
        this.setVisible(true);
		
	}
	
	public void setDados(String[][] dados){
		this.dados = dados;
	}
	

    public String getValueAt(int row, int col) {
        return data.get(row)[col];
    }

    
    public void limpaFormSearch(){
    	lblID.setBounds(0,0,0,0);
		txtID.setBounds(0,0,0,0);
		btnSearch.setBounds(0,0,0,0);
		btnInsert.setBounds(0,0,0,0);
    }
    
    public void limpaFormInsert(){
    	lblUsuario.setBounds(0, 0, 0, 0);
		txtUsuario.setBounds(0, 0, 0, 0);
		lblNome.setBounds(0, 0, 0, 0);
		txtNome.setBounds(0, 0, 0, 0);
		lblEmail.setBounds(0, 0, 0, 0);
		txtEmail.setBounds(0, 0, 0, 0);
		lblSenha.setBounds(0, 0, 0, 0);
		txtSenha.setBounds(0, 0, 0, 0);
		lblNivel.setBounds(0, 0, 0, 0);
		txtNivel.setBounds(0, 0, 0, 0);
		btnInsert.setBounds	(0, 0, 0, 0);
		btnUpdate.setBounds(0,0,0,0);
    }
    
    public void limpaFormUpdate(){
		lblNomeUp.setBounds(0, 0, 0, 0);
		txtNomeUp.setBounds(0, 0, 0, 0);
		lblEmailUp.setBounds(0, 0, 0, 0);
		txtEmailUp.setBounds(0, 0, 0, 0);
		lblSenhaUp.setBounds(0, 0, 0, 0);
		txtSenhaUp.setBounds(0, 0, 0, 0);
		lblNivelUp.setBounds(0, 0, 0, 0);
		txtNivelUp.setBounds(0, 0, 0, 0);
		btnUpdate.setBounds(0,0,0,0);
    }
    

    public boolean isDigit(String s) {
        char ch = s.charAt(0);
        return (ch >= 48 && ch <= 57);
    }
    
    
	public static void main(String[] args) {
		new FrmListUsuarios();
	}
}
