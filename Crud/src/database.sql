
CREATE DATABASE gu3003671 CHARSET utf8;

USE gu3003671;
 
CREATE TABLE clientes (
  idCliente 	int(11) 	NOT NULL AUTO_INCREMENT,
  nome 			varchar(50) NOT NULL,
  email 		varchar(32) NOT NULL,
  senha 		varchar(11) NOT NULL,
  PRIMARY KEY (idCliente)
);

INSERT INTO clientes (nome, email, senha) values ('consumidor','consumidor@localhost','senha');

CREATE TABLE produtos (
	idProduto 	int(11) 		NOT NULL AUTO_INCREMENT,
	nome 		varchar(50) 	NOT NULL,
	marca		varchar(50) 	NOT NULL,
	modelo		varchar(60) 	NOT NULL,
	descr 		varchar(200) 	NOT NULL,
	pathImagem	varchar(200),
	PRIMARY KEY (idProduto)
) ;

CREATE TABLE estoque (
  idEstoque 		int(11) 	NOT NULL AUTO_INCREMENT,
  idProduto 		int(11),
  notaFiscal		varchar(100), -- ou lote
  dataEntrada		varchar(10),
  qtd				int(11),
  dataValidade		varchar(10),
  fluxo				int(11), -- 1 entrada , -1 saída,
  fluxoMotivo		varchar(50),
  PRIMARY KEY (idEstoque),
  FOREIGN KEY (idProduto) REFERENCES produtos ( idProduto)
);

CREATE TABLE preco (
  idPreco 	int(11) 	NOT NULL AUTO_INCREMENT,
  idProduto	int(11),
  data		varchar(10), 
  valor		numeric(11,2),	
  PRIMARY KEY (idPreco),
  FOREIGN KEY (idProduto) REFERENCES produtos ( idProduto)
) ;

CREATE TABLE venda(
  idVenda 	int(11) 	NOT NULL AUTO_INCREMENT,
  data		varchar(10), 
  idCliente	int(11)	default 1,
  idProduto int(11),
  idPreco 	int(11),
  PRIMARY KEY (idVenda),
  FOREIGN KEY (idProduto) REFERENCES produtos ( idProduto)
) ;


CREATE TABLE usuarios(
  idUsuario int(11) 	NOT NULL AUTO_INCREMENT,
  nome		varchar(30), 
  email		varchar(50),
  senha 	varchar(20),
  nivel 	int(11) default 1,
  PRIMARY KEY (idUsuario)
);






















