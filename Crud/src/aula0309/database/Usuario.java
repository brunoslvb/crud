package aula0309.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario {
	
	private String idUsuario; 
	private String nome;
	private String email; 
	private String senha; 
	private String nivel;
	
	private String tableName	= "gu3003671.usuarios";
	private String fieldsName 	= "idUsuario, nome, email, senha, nivel";
	private String fieldKey		= "idUsuario";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	
	public Usuario(String idUsuario, String nome, String email, String  senha, String nivel) {
		this.idUsuario = idUsuario;
		this.nome      = nome;
		this.email     = email;
		this.senha     = senha;
		this.nivel     = nivel;
	}

	private String[] toArray() {
		return(
			new String[]{
				this.idUsuario, 
				this.nome,
				this.email,
				this.senha,
				this.nivel				
			}
		);
	}
	
	public String toString() {
		return(
			"\n\t "+
			this.idUsuario+", "+ 
			this.nome+", "+ 
			this.email+", "+ 
			this.senha+", "+ 
			this.nivel+", "		
		);
	}
	
	public void save() {
		if (this.idUsuario == ""){
			this.idUsuario = "0";
			dbQuery.insert(this.toArray());
		}else{
			dbQuery.update(this.toArray());
		}
	}
	
	
	public String[][] resultSetToArray( ResultSet rs ) {
		
		try {
			
			int qtdColumns = rs.getMetaData().getColumnCount();
			int qtdLines   = 0;
			
			rs.beforeFirst();
			while ( rs.next() ){
				qtdLines  += 1; 
			}
			
			
			String[][] tmp_dados = new String[qtdLines][qtdColumns];
			rs.beforeFirst();
			for (int lin = 0; lin < qtdLines; lin++) {
				rs.next();
				for (int col = 0; col < qtdColumns; col++) {
					tmp_dados[lin][col] = rs.getString(col+1);
				}	
			}
			return (tmp_dados);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}
	
	
	public boolean delete() {
		if (this.idUsuario != null){
			dbQuery.delete(this.toArray());
			try{
				return ( true );
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		 	return( false );
		}
		return false;
	}
	
	public boolean checkLogin() {
		
		ResultSet rs =	dbQuery.select(" (nome = '"+this.nome+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
	 	try {
	 		return( rs.next() );
		 /*
			if ( rs.next() ){
			 return( true );
		 	}	else {
			 	return( false );
		 	}
		 */
	 	} catch (SQLException e) {
			e.printStackTrace();
		}
	 	return( false );
	}

	public ResultSet listall() {
		return(dbQuery.select(""));
	}
	
	public ResultSet find() {
		return(dbQuery.select("idUsuario = '"+this.idUsuario+"'"));
	}
	
	public ResultSet userlogged() {
		return(dbQuery.select("nome = '"+this.nome+"' or email = '"+this.email+"'"));
	}
	
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
}
