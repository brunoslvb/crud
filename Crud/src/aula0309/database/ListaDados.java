package aula0309.database;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import aula0309.database.Usuario;
 

public class ListaDados extends JFrame {
	
	private JLabel			lblID 	= new JLabel("ID: ");
	private JTextField 		txtID 	= new JTextField();
	private JButton			btnConfirm 	= new JButton("Buscar");
	private JLabel			lblMsg 		= new JLabel("____________________________");
	
	public void FrmListUsuarios() {
		
		this.setTitle("Resultado da busca");
		this.setBounds(250, 250, 600, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);

		lblID.setBounds( 5,  5, 100, 30);
		txtID.setBounds( 50, 5, 150, 30);
		lblMsg.setBounds(20, 305, 250, 30);
		btnConfirm.setBounds(50, 255, 150, 30);
		
		this.add(lblID);
		this.add(txtID);
		this.add(lblMsg);
		this.add(btnConfirm);
		
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Usuario usuario = new Usuario(txtID.getText(), "", "", "", "");
				ListaDados ld = new ListaDados();
				ld.criaJanela(txtID.getText());
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public void criaJanela(String ID){
         
    	this.setTitle("Lista de Usuarios");
		this.setBounds(500, 500, 600, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		btnConfirm.setText("Deletar");
		
		lblMsg.setBounds(20, 305, 250, 30);
		btnConfirm.setBounds(50, 255, 150, 30);
		
		System.out.print(ID);
		
		Usuario usuario = new Usuario(ID, "", "", "", "");
		
		String[] cabecalho = {"idUsuario","nome","email","senha","nivel"};
		String[][] dados   =  usuario.resultSetToArray(usuario.find());
		JTable jTable = new JTable(dados,cabecalho);
		jTable.setBounds(5, 5, 300, 100);
	
		this.add(jTable);
		this.add(btnConfirm);
		this.add(lblMsg);
		
		
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Usuario usuario = new Usuario(txtID.getText(), "", "", "", "");
				ListaDados ld = new ListaDados();
				ld.Delete(ID);
			}
		});
		
		
		this.setVisible(true);
    }
    

	public void Delete(String ID){
		
		Usuario usuario = new Usuario(ID, "", "", "", "");
				
		//System.out.print(usuario.find());
		//ResultSet rs = usuario.find();
		
//		try {
//			while(rs.next()){
//				System.out.print(rs.getString("nome"));
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		if(usuario.delete()){
			//lblMsg.setText("Usu�rio " + ID + "foi exclu�do com sucesso.");
			System.out.print("Usu�rio " + ID + "foi exclu�do com sucesso.");
		} else {
			//lblMsg.setText("Erro ao excluir");
			System.out.print("Erro ao excluir");
		}
	}
     
//  public Object [][] getDados(){
//	Usuario usuario = new Usuario("", "", "", "", "");
//	    	
//	try {
//		
//		String[] cabecalho = {"idUsuario","nome","email","senha","nivel"};
//		String[][] dados   =  this.resultSetToArray(new Usuario("", "", "", "", "").listall());
//		
//		ResultSet rs = usuario.listall();
//		
//		while ( rs.next() ){
//							
//			dados2 = new Object[][] {{rs.getString("idUsuario"), rs.getString("nome"), rs.getString("email"), rs.getString("senha"), rs.getString("nivel")},};
//			
//			//System.out.print(dados2);
//			
//			System.out.print(
//				"\n"+
//				rs.getString("idUsuario") + " | " +
//				rs.getString("nome") + " | " +
//				rs.getString("email") + " | " +
//				rs.getString("senha") + " | " +
//				rs.getString("nivel") 
//			);		
//		}
//	
//	} catch (SQLException e) {
//		System.out.print("Verifique o comando ou a dependencia de chave extrangeira!");
//	}
//	
//	return dados2;
//}
	
	
	
    public static void main(String[] args) {
        
    	ListaDados ld = new ListaDados();
    	ld.FrmListUsuarios();
    }
	
}
