package mail;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.SimpleEmail;
 
public class Mail{
	
	private String user;
	private String email;
	private String passwd;
	
	public Mail(String user, String email, String passwd) {
		this.user = user;
		this.email = email;
		this.passwd = passwd;
	}
	
	public void sendMail(){
		String meuEmail = "";
		String senha = "";
		
		SimpleEmail email = new SimpleEmail();
		email.setHostName("smtp.zoho.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator(meuEmail, senha));
		email.setSSLOnConnect(true);
		
		try {
			email.setFrom(meuEmail);
			email.setSubject("Teste de e-mail - JAVA");
			email.setMsg("Ol� "+this.user+",\n\nEste � um e-mail de confirma��o. \nSua conta foi criada com sucesso em um programa desenvolvido na \nlinguagem de programa��o Java. \n\nInforma��es de acesso:\nUsu�rio: "+this.user+"\nSenha: "+this.passwd+" \n\nAtenciosamente, \nBruno da Silva Barros\nGU3003671");
			email.addTo(this.email);
			email.send();
			System.out.print("E-mail enviado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("Falha ao enviar o e-mail.");
		}		
	}

//	public static void main(String[] args) {
//		Mail mail = new Mail("brunosilva2365@gmail.com");
//		mail.sendMail();
//	}  
}
